# Imagen Base
FROM openjdk:8-jdk-alpine

# Author
MAINTAINER "desarrollo@acardenas.com.pe"

# Argumentos
ARG ZONA_HORARIA
ARG JAR_FILE=target/*.jar

# Enviroment
ENV TZ=${ZONA_HORARIA}

# Configurar Zona Horaria
RUN apk add --no-cache tzdata
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Puertos expuestos
EXPOSE 80
EXPOSE 8000

# Volumen temporal donde Spring Boot crea directorios de trabajo para Tomcat de forma predeterminada. 
VOLUME /tmp

# Agrega la ruta del jar
ADD ${JAR_FILE} /home/app.jar

# Run
ENTRYPOINT java $JAVA_OPTS \
                -agentlib:jdwp=transport=dt_socket,address=8000,server=y,suspend=n \
                -Djava.security.egd=file:/dev/./urandom \
                -jar /home/app.jar
