# Registro de mensaje

Servicio para registrar mensajes

### Introducción



## Configuración

El servicio registra mensajes de tipo: **Message, warning o error**. También tiene tres tipos de registro: **Database, Console o File**.

#### Configuración de variables de entorno

Para levantar el proyecto debe primero configurar el archivo `.env` que contiene las variables de entorno necesarias para que funcionen algunos endpoints.

- Instrucciones para configurar archivo `.env` en perfil **local**

    - Click derecho sobre el *proyecto -> Properties -> Actions*

    - Agregar los parametros para la opcion ***Run project*** y ***Debug Project***
  
    - NetBeans
  ![Imagen Referencial](.readme_img/netbeans_local_conf.png)
    - Intellij IDEA
      ![Imagen Referencial](.readme_img/intellij_IDEA_local_conf.png)

- Instrucciones para configurar la base de datos `message_log` y la tabla **tb_message**

  Crear la tabla con la siguiente script:

  ```bash
  # -----------------
  # message_log.tb_message 
  # -----------------
  CREATE TABLE IF NOT EXISTS message_log.tb_message(
  Id int NOT NULL AUTO_INCREMENT,
  type_message varchar(50) DEFAULT NULL,
  date_creation timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (Id)
  )

  ```

#### 

### Cómo iniciar
**Nota** este proyecto requiere  **java >=v1.8**.

En orden para iniciar el proyecto:
```bash
$git clone git@gitlab.com:acrdeveloper/message-log-api.git

# iniciar el proyecto en modo local 
$ mvn clean install 
# ejecutar proyecto desde netbeans o intellij

# iniciar el proyecto en modo docker
$ ./preparate.sh run
```
#### 

### Cómo consumir el servicio
- Ambiente local
    ```bash
    Método -> POST 
    Ruta   -> http://localhost:8086/message-log/messages/log
    ```
  - Body
  ```bash
  {
  "logError": true,
  "logToDatabase": true
  }
  ```
  - Repuesta 
   ```bash
  Status: 200
  
  {
  "path": "/message-log/messages/log",
  "status": 200,
  "data": 1
  }
  ```
    ```bash
  Status: 400

  {
      "path": "/message-log/messages/log",
      "status": 400,
      "apiError": {
        "typeClass": "com.acardenas.message.log.exception.ValidacionException",
        "message": "Error or Warning or Message must be specified",
        "errorTrack": {
          "methodName": "log",
          "fileName": "MessageServiceBean.java",
          "lineNumber": 33,
          "className": "com.acardenas.message.log.service.MessageServiceBean",
          "nativeMethod": false
        }
      }
  }
  ```