package com.acardenas.message.log.exception;

public class ValidacionException extends Exception {

    public ValidacionException() {
    }

    public ValidacionException(String mensaje) {
        super(mensaje);
    }
    
    public ValidacionException(String mensaje, Exception e) {
        super(mensaje,e);
    }
}
