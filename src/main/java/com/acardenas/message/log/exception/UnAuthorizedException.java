
package com.acardenas.message.log.exception;

public class UnAuthorizedException extends Exception {

    public UnAuthorizedException() {
    }

    public UnAuthorizedException(String mensaje) {
        super(mensaje);
    }
    public UnAuthorizedException(String mensaje, Exception e) {
        super(mensaje,e);
    }
}
