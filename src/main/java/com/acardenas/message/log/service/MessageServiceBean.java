package com.acardenas.message.log.service;

import com.acardenas.message.log.common.C;
import com.acardenas.message.log.exception.ValidacionException;
import com.acardenas.message.log.mapper.MessageMapper;
import com.acardenas.message.log.model.LogModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class MessageServiceBean implements MessageService {
    private static final Logger logger = LogManager.getLogger(MessageServiceBean.class);

    private final MessageMapper messageMapper;

    @Autowired
    public MessageServiceBean(MessageMapper messageMapper) {
        this.messageMapper = messageMapper;
    }

    @Override
    public int log(LogModel logModel) throws Exception {
        int reply = 0;

        HashMap hashMap = new HashMap<>();
        if (logModel.isLogToDatabase()) {

            if (typeMessage(logModel) == "") {
                throw new ValidacionException("Error or Warning or Message must be specified");
            }

            hashMap.put("typeMessage", typeMessage(logModel));
            reply = messageMapper.setMessage(hashMap);

        } else if (logModel.isLogToConsole()) {

            if (typeMessage(logModel) == "") {
                throw new ValidacionException("Error or Warning or Message must be specified");
            }
            switch (typeMessage(logModel)) {
                case C.MESSAGE:
                    logger.info(C.MESSAGE);
                case C.WARNING:
                    logger.warn(C.WARNING);
                default:
                    logger.error(C.ERROR);
            }
            reply = 1;

        } else if (logModel.isLogToFile()) {
            throw new ValidacionException("functionality under development");
        } else {
            throw new ValidacionException("specify the output type to display the message");
        }

        return reply;
    }

    //Private methods
    //<editor-fold defaultstate="collapsed" desc="private methods">
    private String typeMessage(LogModel logModel) {
        String type = "";
        if (logModel.isLogMessage()) {
            type = C.MESSAGE;
        } else if (logModel.isLogWarning()) {
            type = C.WARNING;
        } else if (logModel.isLogError()) {
            type = C.ERROR;
        }
        return type;
    }
    //</editor-fold>
}
