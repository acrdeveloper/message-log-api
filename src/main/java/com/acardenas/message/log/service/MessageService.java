package com.acardenas.message.log.service;

import com.acardenas.message.log.model.LogModel;

public interface MessageService {

    int log(LogModel logModel) throws Exception;
}
