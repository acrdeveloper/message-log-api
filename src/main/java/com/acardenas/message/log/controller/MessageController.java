package com.acardenas.message.log.controller;

import com.acardenas.message.log.model.LogModel;
import com.acardenas.message.log.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/messages")
@RestController
public class MessageController {

    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @PostMapping(path = "/log",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> log(@RequestBody LogModel logModel) throws Exception {

        return ResponseEntity
                .ok()
                .body(messageService.log(logModel));

    }
}
