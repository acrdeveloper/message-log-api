package com.acardenas.message.log.handler;


import com.acardenas.message.log.exception.UnAuthorizedException;
import com.acardenas.message.log.exception.ValidacionException;
import com.acardenas.message.log.pojo.ExceptionResponse;
import com.acardenas.message.log.pojo.ObjectResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Stream;


@ControllerAdvice
public class RestExceptionHandler {

    //exceptions a tratar
    @ExceptionHandler({ Throwable.class})
    public ResponseEntity<?> exceptionCatcher(HttpServletRequest request, Exception ex) {
        //armado de respuesta de una exception
        HttpStatus status           = HttpStatus.INTERNAL_SERVER_ERROR;
        StackTraceElement traceElement = Stream.of(ex.getStackTrace()).findFirst().get();

        if (ex instanceof UnAuthorizedException) {
            status = HttpStatus.UNAUTHORIZED;
        }

        if(ex instanceof ValidacionException){
            status = HttpStatus.BAD_REQUEST;
        }

        ObjectResponse body =  ObjectResponse.builder()
                                .path(request.getRequestURI())
                                .apiError(
                                        ExceptionResponse.builder()
                                                .typeClass(ex.getClass().getName())
                                                .message(ex.getMessage())
                                                .errorTrack(traceElement).build())
                                .status(status.value()).build();

        return new ResponseEntity<>(body, status);
    }
}
