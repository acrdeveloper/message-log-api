package com.acardenas.message.log.handler;

import com.acardenas.message.log.pojo.ObjectResponse;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;
import java.util.stream.Stream;

@Aspect
@Component
public class RestAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestAspect.class);

    @Pointcut("execution(public * com.acardenas.message.log.controller.*.*(..))")
    public void allControllers() {
    }

    @Pointcut("execution(public * com.acardenas.message.log.service.*.*(..))")
    public void allServices() {
    }

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private HttpServletResponse response;


    @Before("allControllers()")
    public void beforeCallController(JoinPoint jp) {
        String metodo = obtenerNombreCompletoMetodo(jp);
        LOGGER.info(">>>> METODO LLAMADO: {}", metodo.toUpperCase());

        StringBuilder argsString = new StringBuilder();
        Object[] args = jp.getArgs();

        if (args.length > 0) {
            for (int i = 0; i < args.length; i++) {
                argsString.append(String.format("[Arg-%s: %s]", (i + 1), args[i]));
            }

            LOGGER.info(">>>> ARGUMENTOS: {}", argsString.toString());
        }
    }

    @Around("allControllers()")
    public Object maskResponseController(ProceedingJoinPoint joinPoint) throws Throwable {

        long inicio = System.currentTimeMillis();

        Object objResultado = joinPoint.proceed();
        int status = response.getStatus();

        if (objResultado instanceof ResponseEntity) {

            ResponseEntity<?> preResultado = (ResponseEntity<?>) objResultado;

            if (!(preResultado.getBody() instanceof InputStreamResource)){

                status = preResultado.getStatusCodeValue();
                objResultado = preResultado.getBody();

                //resultado final
                ObjectResponse<?> body = ObjectResponse.builder()
                        .path(request.getRequestURI())
                        .status(status)
                        .data(objResultado).build();

                objResultado = ResponseEntity.status(status).body(body);
            }

        }

        long fin = System.currentTimeMillis();

        LOGGER.info("Tiempo de ejecución: {} ms", (fin - inicio));

        return objResultado;
    }

    @Around("allServices()")
    public Object showLogs(ProceedingJoinPoint joinPoint) throws Throwable {
        String metodo = obtenerNombreCompletoMetodo((JoinPoint) joinPoint);
        LOGGER.info(">>>> INICIO: {}", metodo);

        Object resultado = joinPoint.proceed();

        LOGGER.info("<<<< FIN: {}", metodo);
        return resultado;

    }

    @AfterThrowing(pointcut = "allControllers()", throwing = "ex")
    public void exceptionAfertCallController(JoinPoint jp, Exception ex) {
        Optional<StackTraceElement> optTraceElement = Stream.of(ex.getStackTrace())
                .findFirst();

        String mensaje = String.format("[Mensaje: %s] - [Tipo: %s] - [Archivo: %s] - [Metodo: %s] - [Linea: %s]",
                ex.getMessage(), ex.getClass(),
                optTraceElement.map(StackTraceElement::getFileName).orElse("N/E"),
                optTraceElement.map(StackTraceElement::getMethodName).orElse("N/E"),
                optTraceElement.map(StackTraceElement::getLineNumber)
                        .map(String::valueOf).orElse("N/E"));

        LOGGER.error(">>>> OCURRIO UNA EXCEPCIÓN: {}", mensaje, ex);
    }

    private static String obtenerNombreMetodo(JoinPoint jp) {
        return Optional.ofNullable(jp.getSignature())
                .map(Signature::getName)
                .orElse("[no existe metodo]");
    }

    private static String obtenerNombreCompletoMetodo(JoinPoint jp) {
        return Optional.ofNullable(jp.getSignature())
                .map(Signature::toShortString)
                .orElse("[no existe metodo]");
    }

}
